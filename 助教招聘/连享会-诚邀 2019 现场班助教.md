「[2019 暑期 Stata 现场班](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)」（[微信版](https://mp.weixin.qq.com/s/7heG1VrmUtuV74iWhEi93A)）即将开始。为保证本次课程的全方位答疑解惑，现招聘培训课程助手数名，主要辅助老师的教学和答疑工作。助教可以免费参加相应班次的培训课程。

>#### 具体说明和要求：

- **名额：** 9 名 (初级班、高级班和论文班各 3 名)
- **任务：** (1) 做好开课前的准备工作，撰写 3-5 篇介绍 Stata 和计量经济学基础知识的文档；(2) 协助老师的教学工作；(3) 负责课间和课后答疑，以及课程总结推文编辑工作 (Note: 下午 5:00-5:30 的现场答疑由主讲教师负责)。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录。
- **申请截止时间：** 2019 年 6 月 10 日 (将于 2019.6.12 日公布遴选结果)
 
>#### 申请方式：在线提交表单

助教申请网址：https://www.wjx.top/jq/40454721.aspx 

亦可扫码填写助教申请资料：

![连享会-2019暑期Stata现场班助教招聘-扫码填写资料](https://images.gitee.com/uploads/images/2019/0530/172203_35f72a4e_1522177.png "连享会-2019暑期Stata现场班助教招聘-扫码填写资料")

&emsp;

### 附：2019 暑期 Stata 现场班课程简介
---
#### A. 课程概要

> **时间：** 2019 年 7 月 17-26 日    
> **地点：** 北京 首都师范大学-国际文化大厦    
> **授课教师：** 初级+高级：连玉君 (6天)  ||  论文班：刘瑞明(1天)+连玉君(2天)

- **Stata暑期研讨[全程班](http://www.peixun.net/view/1224.html)** 
   - 时间地点：2019 年 7 月 17 日-26 日，北京
   - 课程链接：[http://www.peixun.net/view/1224.html](http://www.peixun.net/view/1224.html)
   
- **Stata暑期研讨[初级班](http://www.peixun.net/view/307_detail.html)**
   - 时间地点：2019 年 7 月 17 日-19 日 (三天)，北京
   - 课程链接：[http://www.peixun.net/view/307_detail.html](http://www.peixun.net/view/307_detail.html)

- **Stata暑期研讨[高级班](http://www.peixun.net/view/308_detail.html)**
   - 时间地点：2019 年 7 月 21 日- 23 日 (三天)，北京
   - 课程链接：[http://www.peixun.net/view/308_detail.html](http://www.peixun.net/view/308_detail.html)

- **Stata暑期研讨[论文班](http://www.peixun.net/view/1135.html)**
   - 时间地点：2019 年 7 月 24 日- 26 日 (三天)，北京
   - 课程链接：[http://www.peixun.net/view/1135.html](http://www.peixun.net/view/1135.html)


#### B. 讲授嘉宾简介

**[连玉君](http://lingnan.sysu.edu.cn/node/151)**，经济学博士，副教授，博士生导师。2007年7月毕业于西安交通大学金禾经济研究中心，现任教于中山大学岭南学院金融系。主讲课程为“金融计量”、“计量分析与Stata应用”、“实证金融”等。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》等期刊发表论文60余篇。连玉君副教授主持国家自然科学基金项目（2项）、教育部人文社科基金项目、广东自然科学基金项目等课题项目10余项。目前已完成Panel VAR、Panel Threshold、Two-tier Stochastic Frontier等计量模型的Stata实现程序，并编写过几十个小程序，如xtbalance、winsor2、bdiff、hausmanxt、ttable3、hhi5等。

**[刘瑞明](http://nads.ruc.edu.cn/displaynews.php?id=4942)**，复旦大学经济学博士，中国人民大学国家发展与战略研究院教授、博导。主要研究转型经济学、发展经济学、产业经济学，近年来以独立作者和第一作者身份在《经济研究》、《管理世界》、《经济学季刊》、《世界经济》、《中国工业经济》等杂志发表论文  40 余篇，主持国家自然科学基金面上项目、教育部人文社科规划项目等多项国家级、省部级项目，获得“教育部第七届中国高校科学优秀成果奖”、“全国优秀博士论文提名奖”、“第六届黄达-蒙代尔经济学奖”、“教育部博士研究生学术新人奖”、“首届谭崇台发展经济学奖”等多项学术荣誉。


&emsp;



[![2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](http://upload-images.jianshu.io/upload_images/7692714-2e573f9e9302e8f9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "imgNum_5102103")](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)

