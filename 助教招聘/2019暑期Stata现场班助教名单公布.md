
[![2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://upload-images.jianshu.io/upload_images/7692714-321b9566d51e2db2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)

&emsp;


> #### [2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)


&emsp;



承蒙各位的关爱和支持，自 「[2019 暑期 Stata 现场班](https://links.jianshu.com/go?to=https%3A%2F%2Fgitee.com%2Farlionn%2FCourse%2Fblob%2Fmaster%2F2019%25E6%259A%2591%25E6%259C%259FStata%25E7%258E%25B0%25E5%259C%25BA%25E7%258F%25AD.md)」（[微信版](https://links.jianshu.com/go?to=https%3A%2F%2Fmp.weixin.qq.com%2Fs%2F7heG1VrmUtuV74iWhEi93A)）以及 [连享会 - 诚邀 2019 暑期 Stata 现场班助教](https://www.jianshu.com/p/aeb67c959082)  发布以来，我们陆续接到了来自 **30 余所** 高校 **40 余位** 老师和同学的申请。

非常感谢大家对连享会现场培训的关注和支持。

经过主办方和任课老师的遴选，最终确定如下 9 位申请者担任本次培训的助教：

> **Stata暑期研讨 [初级班](http://www.peixun.net/view/307_detail.html)** 助教：
- 何庆红（北京大学中国卫生经济研究中心）；
- 黄俊凯（中国人民大学）；
- 徐婷（对外经济与贸易大学）

> **Stata暑期研讨 [高级班](http://www.peixun.net/view/308_detail.html)** 助教：
- 李珍（厦门大学）；
- 万莉（北京航空航天大学）；
- 陈勇吏（上海交通大学）

> **Stata暑期研讨 [论文班](http://www.peixun.net/view/1135.html)** 助教：
- 崔颖（中央财经大学）；
- 胡雨霄（伦敦政治经济学院）；
- 潘星宇（清华大学）

**温馨提示：** 请各位入选的老师和同学收到邮件通知或看到此通知后，发送邮件到 [StataChina@163.com](StataChina@163.com)，确认能否参加此次培训，以便我们及时做出调整，增加新的助教名额。     

邮件要求如下：
- **邮件标题：** 2019Stata培训助教确认-姓名-学校
- **邮件内容：** (1) 确认是否能参加此次培训；(2) 填写您的联系方式 (姓名；手机号；邮箱；微信号)。

**助教遴选的基本规则如下：**
(1) 专业或研究背景与本次培训的内容之间的相关度；
(2) 有独立进行实证分析的经验；

对于未能入选担任本次培训助教的各位老师和学生，我们也详细记录了各位的资料，希望在日后的培训中可以邀请各位担任与您知识背景相关的专题的助教工作。

>秉承**「知识需要分享」**的理念，我们也诚挚邀请各位将研究和学习过程中的经验和心得记录下来，写成具有较强可读性和实操性的推文，通过 [简书平台](http://www.jianshu.com/) 投稿到 [Stata连享会-简书](http://www.jianshu.com/u/69a30474ef33) (投稿方法：在简书平台中写好推文，点击页面右下角【**文章投稿**】图标；搜索【**Stata连享会**】；点击投稿)；或通过邮件发送到 [StataChina@163.com](StataChina@163.com)。     
在日后的助教遴选中，我们会优先邀请有投稿经历的老师和同学。

衷心感谢各位的支持和关注！

**特别说明：** 文中包含的链接在微信中无法生效。请点击本文底部左下角的【阅读原文】。


&emsp;

&emsp;


> ### 附：2019暑期Stata现场班课程概要

---
## A. 课程概要


> **时间：** 2019 年 7 月 17-26 日    
> **地点：** 北京 首都师范大学-国际文化大厦    
> **授课教师：** 初级+高级：连玉君 (6天)  ||  论文班：刘瑞明(1天)+连玉君(2天)


- **Stata暑期研讨[全程班](http://www.peixun.net/view/1224.html)** 
   - 时间地点：2019 年 7 月 17 日-26 日，北京
   - 课程链接：[http://www.peixun.net/view/1224.html](http://www.peixun.net/view/1224.html)
   
- **Stata暑期研讨[初级班](http://www.peixun.net/view/307_detail.html)**
   - 时间地点：2019 年 7 月 17 日-19 日 (三天)，北京
   - 课程链接：[http://www.peixun.net/view/307_detail.html](http://www.peixun.net/view/307_detail.html)

- **Stata暑期研讨[高级班](http://www.peixun.net/view/308_detail.html)**
   - 时间地点：2019 年 7 月 21 日- 23 日 (三天)，北京
   - 课程链接：[http://www.peixun.net/view/308_detail.html](http://www.peixun.net/view/308_detail.html)

- **Stata暑期研讨[论文班](http://www.peixun.net/view/1135.html)**
   - 时间地点：2019 年 7 月 24 日- 26 日 (三天)，北京
   - 课程链接：[http://www.peixun.net/view/1135.html](http://www.peixun.net/view/1135.html)


## B. 讲授嘉宾简介

**[连玉君](http://lingnan.sysu.edu.cn/node/151)**，经济学博士，副教授，博士生导师。2007年7月毕业于西安交通大学金禾经济研究中心，现任教于中山大学岭南学院金融系。主讲课程为“金融计量”、“计量分析与Stata应用”、“实证金融”等。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》等期刊发表论文60余篇。连玉君副教授主持国家自然科学基金项目（2项）、教育部人文社科基金项目、广东自然科学基金项目等课题项目10余项。目前已完成Panel VAR、Panel Threshold、Two-tier Stochastic Frontier等计量模型的Stata实现程序，并编写过几十个小程序，如xtbalance、winsor2、bdiff、hausmanxt、ttable3、hhi5等。

**[刘瑞明](http://nads.ruc.edu.cn/displaynews.php?id=4942)**，复旦大学经济学博士，中国人民大学国家发展与战略研究院教授、博导。主要研究转型经济学、发展经济学、产业经济学，近年来以独立作者和第一作者身份在《经济研究》、《管理世界》、《经济学季刊》、《世界经济》、《中国工业经济》等杂志发表论文  40 余篇，主持国家自然科学基金面上项目、教育部人文社科规划项目等多项国家级、省部级项目，获得“教育部第七届中国高校科学优秀成果奖”、“全国优秀博士论文提名奖”、“第六届黄达-蒙代尔经济学奖”、“教育部博士研究生学术新人奖”、“首届谭崇台发展经济学奖”等多项学术荣誉。


&emsp;

&emsp;




>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://upload-images.jianshu.io/upload_images/7692714-8b1fb0b5068487af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)





---
[![欢迎加入Stata连享会(公众号: StataChina)](https://upload-images.jianshu.io/upload_images/7692714-fbec0770ffb974d8.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")](https://gitee.com/arlionn/Course/blob/master/README.md)


