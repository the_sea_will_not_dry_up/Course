
&emsp;
 
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)



---
> ### 连享会 现场班-精品课程
  
    
> - [内生性专题现场班](https://gitee.com/arlionn/Course/blob/master/Done/2019Endog.md) (11月14-17日，南京；林建浩、王群勇主讲)      
> &emsp;  让我爱恨交织的内生性！
> &emsp;    
> - [2020寒假Stata现场班](https://gitee.com/arlionn/Course/blob/master/StataFull.md) (2020年1月8-17日，北京；连玉君、江艇主讲)       
> &emsp; 完整的知识架构是你长期成长的动力源泉……


&emsp;

> **即将发布：**
  - 4 天，**爬虫和文本分析专题班**，主讲嘉宾：司继春(上海对外经贸大学)；游万海(福州大学)


&emsp;


### A. [内生性专题现场班](https://gitee.com/arlionn/Course/blob/master/Done/2019Endog.md)   
- **时间地点：** 2019 年 11 月 14-17 日 (周四-周日)，南京财经大学福州路校区
- **主讲嘉宾：** 林建浩 (中山大学)；王群勇 (南开大学)
- **课程主题：** 内生性问题及估计方法
- **课程要点：** 整体上遵循 **「从文献中来，到论文中去」** 的原则。结合 Top 期刊论文 (AER, JPE, QJE, JoE, JF 等) 讲解目前主流的内生性问题应对方法和模型。在理解理论的基础上，通过 Stata 实操和论文解读来加强对模型的理解和应用。
- **主要方法：** 传统 **IV** 估计；弱 IV 和近似外生 IV；**面板**数据模型；倍分法 (**DID**)：DID, 动态 DID 及三重差分 (DDD)；断点回归 (**RDD**)；倾向得分匹配分析 (**PSM**)；**处理效应**模型 (Treatment Effect model)；内生 Probit/Logit 等
- **软件和课件：** 本次培训使用 Stata 软件。课程中涉及的所有数据、程序和论文实现代码，会在开课前一周发送给学员。
- **课程主页：** [内生性专题现场班](https://gitee.com/arlionn/Course/blob/master/Done/2019Endog.md) 
[![连享会-内生性专题班-报名页-2019.11.14-17](https://images.gitee.com/uploads/images/2019/0908/155222_62e4c77b_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019Spatial.md)


&emsp;

---
### B. [2020寒假Stata现场班](https://mp.weixin.qq.com/s/Yg3UMJj1JLlYoz6D-SAGow)  
- **时间地点：** 2020年1月8-17日；北京-海淀区
- **主讲嘉宾**：连玉君 (中山大学，初级+高级)；江艇 (中国人民大学，论文班)
- **授课内容：** 全面介绍 Stata 数据处理、编程、主流计量方法，通过剖析经典论文掌握论文写作方法
  - **课程要点：** 模型设定、交乘项、静态和动态面板数据模型、面板门槛模型、内生性专题 (DID, PSM, RDD)，合成控制法，Probit模型等。
- **课程主页：** [2020寒假Stata现场班](https://mp.weixin.qq.com/s/Yg3UMJj1JLlYoz6D-SAGow) 

> 扫码报名：

![2020寒假Stata现场班报名二维码](https://images.gitee.com/uploads/images/2019/1023/230506_11cf307f_1522177.png "屏幕截图.png")



